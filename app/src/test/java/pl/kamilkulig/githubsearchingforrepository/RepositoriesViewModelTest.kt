package pl.kamilkulig.githubsearchingforrepository

import org.junit.After
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.mockito.*
import org.mockito.Mockito.verify
import pl.kamilkulig.githubsearchingforrepository.data.RepositoryDataSource.LoadRepositoriesCallback
import pl.kamilkulig.githubsearchingforrepository.data.RepositoryRepository
import pl.kamilkulig.githubsearchingforrepository.data.model.Repository
import pl.kamilkulig.githubsearchingforrepository.data.model.SearchCriteria
import pl.kamilkulig.githubsearchingforrepository.repositories.RepositoriesViewModel

class RepositoriesViewModelTest {

    private lateinit var repositoriesViewModel: RepositoriesViewModel
    @Mock private lateinit var repositoryRepository: RepositoryRepository
    @Captor private lateinit var loadRepositoriesCallbackCaptor: ArgumentCaptor<LoadRepositoriesCallback>
    @Captor private lateinit var searchCriteriaCaptor: ArgumentCaptor<SearchCriteria>
    private lateinit var items: MutableList<Repository>

    private fun <T> capture(argumentCaptor: ArgumentCaptor<T>): T = argumentCaptor.capture()

    @Before fun setupRepositoriesViewModel() {
        MockitoAnnotations.initMocks(this)
        repositoriesViewModel = RepositoriesViewModel(repositoryRepository)
        items = mutableListOf(
                Repository("repo1", "desc"),
                Repository("repo2", "desc"))
    }

    @Test fun searchRepositoriesFromRepository() {
        with(repositoriesViewModel) {
            assertTrue(empty.get())
            assertFalse(dataLoading.get())
            repositoriesViewModel.search()
            assertTrue(dataLoading.get())
            verify(repositoryRepository).getRepositories(capture(searchCriteriaCaptor),
                    capture(loadRepositoriesCallbackCaptor))
            loadRepositoriesCallbackCaptor.value.onRepositoriesLoaded(items)
            assertTrue(repositories.size == 2)
            assertFalse(repositories.isEmpty())
            assertFalse(dataLoading.get())
        }
    }

    @After fun destroyRepositoryInstance() {
        RepositoryRepository.destroyInstance()
    }
}