package pl.kamilkulig.githubsearchingforrepository.repositorydetail

import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.content_repository_detail.*
import kotlinx.android.synthetic.main.activity_repository_detail.*
import pl.kamilkulig.githubsearchingforrepository.R
import pl.kamilkulig.githubsearchingforrepository.data.RetrofitService
import pl.kamilkulig.githubsearchingforrepository.data.model.RepositoryDetail
import pl.kamilkulig.githubsearchingforrepository.repositories.RepositoriesActivity
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RepositoryDetailActivity : AppCompatActivity() {

    private lateinit var repoLink: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repository_detail)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        val fullName = intent.getStringExtra(RepositoriesActivity.INTENT_NAME_REPO).split("/")
        sendRequestToGithubServer(fullName[0], fullName[1])
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun sendRequestToGithubServer(owner: String, name: String) {
        val retrofit: Retrofit = Retrofit.Builder()
                .baseUrl("https://api.github.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        val retrofitService = retrofit.create(RetrofitService::class.java)
        val call = retrofitService.getRepositoriesDetail(owner, name)
        call.enqueue(object : Callback<RepositoryDetail> {
            override fun onFailure(call: Call<RepositoryDetail>?, t: Throwable?) {
                Toast.makeText(this@RepositoryDetailActivity, "network failure :(", Toast.LENGTH_SHORT).show()
                progress_bar.visibility = View.GONE
            }

            override fun onResponse(call: Call<RepositoryDetail>?, response: Response<RepositoryDetail>) {
                val responseBody = response.body()
                if (responseBody != null) {
                    updateView(responseBody)
                }
            }
        })
    }

    private fun updateView(repo: RepositoryDetail) {
        repository_detail_linear_layout.visibility = View.VISIBLE
        progress_bar.visibility = View.GONE
        val fullName = repo.name.split("/")
        owner_text_view.text = fullName[0]
        name_text_view.text = fullName[1]
        desc_text_view.text = repo.description
        stars_text_view.text = "Stars " + repo.stars
        forks_text_view.text = "Forks " + repo.forks
        watchers_text_view.text = "Watchers " + repo.watchers
        repoLink = repo.url
    }

    fun goToBrowserOnClick(v: View) {
        startActivity(Intent(Intent.ACTION_VIEW).setData(Uri.parse(repoLink)))
    }
}
