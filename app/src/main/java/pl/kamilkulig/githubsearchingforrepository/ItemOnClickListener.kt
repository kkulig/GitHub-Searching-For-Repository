package pl.kamilkulig.githubsearchingforrepository

interface ItemOnClickListener {
    fun getItemNameOnClick(name: String)
}