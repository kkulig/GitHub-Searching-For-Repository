package pl.kamilkulig.githubsearchingforrepository

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.support.annotation.VisibleForTesting
import pl.kamilkulig.githubsearchingforrepository.data.RepositoryRepository
import pl.kamilkulig.githubsearchingforrepository.repositories.RepositoriesViewModel

class ViewModelFactory(private val repository: RepositoryRepository) :
        ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>) =
            with(modelClass) {
                when {
//                    isAssignableFrom(StatisticsViewModel::class.java) ->
//                        StatisticsViewModel(application, tasksRepository)
//                    isAssignableFrom(TaskDetailViewModel::class.java) ->
//                        TaskDetailViewModel(application, tasksRepository)
//                    isAssignableFrom(AddEditTaskViewModel::class.java) ->
//                        AddEditTaskViewModel(application, tasksRepository)
                    isAssignableFrom(RepositoriesViewModel::class.java) ->
                        RepositoriesViewModel(repository)
                    else ->
                        throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
                }
            } as T

    companion object {

        @Volatile private var INSTANCE: ViewModelFactory? = null

        fun getInstance() =
                INSTANCE ?: synchronized(ViewModelFactory::class.java) {
                    INSTANCE ?: ViewModelFactory(
                            InjectionRepo.provideRepository())
                            .also { INSTANCE = it }
                }


        @VisibleForTesting fun destroyInstance() {
            INSTANCE = null
        }
    }
}