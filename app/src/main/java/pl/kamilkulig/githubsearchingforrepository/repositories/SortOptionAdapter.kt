package pl.kamilkulig.githubsearchingforrepository.repositories

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.item_sort.view.*
import pl.kamilkulig.githubsearchingforrepository.ItemOnClickListener
import pl.kamilkulig.githubsearchingforrepository.R

class SortOptionAdapter(private val sortOptions: List<String>,
                        var itemListener: ItemOnClickListener)
    : RecyclerView.Adapter<SortOptionAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_sort, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int = sortOptions.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.nameTextView.text = sortOptions[position]
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameTextView: TextView = itemView.sort_name_text_view

        init {
            nameTextView.setOnClickListener {
                val name = nameTextView.text.toString()
                itemListener.getItemNameOnClick(name)
            }
        }
    }
}
