package pl.kamilkulig.githubsearchingforrepository.repositories

import android.arch.lifecycle.ViewModel
import android.databinding.ObservableArrayList
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.databinding.ObservableList
import android.os.Bundle
import pl.kamilkulig.githubsearchingforrepository.ItemOnClickListener
import pl.kamilkulig.githubsearchingforrepository.SingleLiveEvent
import pl.kamilkulig.githubsearchingforrepository.data.RepositoryDataSource
import pl.kamilkulig.githubsearchingforrepository.data.RepositoryRepository
import pl.kamilkulig.githubsearchingforrepository.data.model.Repository
import pl.kamilkulig.githubsearchingforrepository.data.model.SearchCriteria
import pl.kamilkulig.githubsearchingforrepository.data.model.SortOption

class RepositoriesViewModel(val repositoryRepository: RepositoryRepository) : ViewModel(),
        SortOptionPickerFragment.OnSortOptionPass, ItemOnClickListener {

    val empty = ObservableBoolean(true)
    val dataLoading = ObservableBoolean(false)
    val keyWords = ObservableField<String>("")
    private val sortOptions: List<SortOption> =
            listOf(SortOption("Best match", "", ""),
                    SortOption("Recently updated", "updated", "desc"),
                    SortOption("Least recently updated", "updated", "asc"),
                    SortOption("Most forks", "stars", "desc"),
                    SortOption("Fewest forks", "stars", "asc"),
                    SortOption("Most stars", "stars", "desc"),
                    SortOption("Fewest stars", "stars", "asc"))
    val sortOption = ObservableField<String>(sortOptions[0].name)
    val searchEvent = SingleLiveEvent<Void>()
    val getRepositoryDetailsEvent = SingleLiveEvent<String>()
    val selectSortOptionEvent = SingleLiveEvent<Bundle>()
    val messageEvent = SingleLiveEvent<String>()
    val repositoriesAdapter: RepositoriesAdapter = RepositoriesAdapter(this)
    val items: ObservableList<Repository> = ObservableArrayList()

    fun search() {
        if (keyWords.get() != ""){
            val sort = sortOptions.single { it.name == sortOption.get() }.value
            val order = sortOptions.single { it.name == sortOption.get() }.order
            searchRepositories(SearchCriteria(keyWords.get().toString(), sort, order))
            searchEvent.call()
        }
    }

    fun openSortOptionsPicker() {
        val bundle = Bundle()
        bundle.putStringArrayList(RepositoriesActivity.ARRAY_KEY, getNamesFromSortOptionList())
        selectSortOptionEvent.value = bundle
    }

    override fun onSortOptionPass(sortOption: String) {
        this.sortOption.set(sortOption)
        search()
    }

    override fun getItemNameOnClick(name: String) {
        getRepositoryDetailsEvent.value = name
    }

    private fun getNamesFromSortOptionList() = ArrayList<String>(sortOptions.map { it.name })

    private fun searchRepositories(searchCriteria: SearchCriteria) {
        dataLoading.set(true)
        repositoryRepository.getRepositories(searchCriteria, object : RepositoryDataSource.LoadRepositoriesCallback {

            override fun onRepositoriesLoaded(items: List<Repository>) {
                with(items) {
                    this@RepositoriesViewModel.items.clear()
                    this@RepositoriesViewModel.items.addAll(items)
                    empty.set(isEmpty())
                }
                dataLoading.set(false)
            }

            override fun onDataNotAvailable() {
                empty.set(true)
                dataLoading.set(false)
                messageEvent.value = "network failure :("
            }
        })
    }
}
