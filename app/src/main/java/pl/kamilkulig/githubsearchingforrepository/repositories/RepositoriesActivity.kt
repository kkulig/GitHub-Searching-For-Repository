package pl.kamilkulig.githubsearchingforrepository.repositories

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.databinding.DataBindingUtil
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import kotlinx.android.synthetic.main.repositories_activity.*
import pl.kamilkulig.githubsearchingforrepository.ItemOnClickListener
import pl.kamilkulig.githubsearchingforrepository.R
import pl.kamilkulig.githubsearchingforrepository.ViewModelFactory
import pl.kamilkulig.githubsearchingforrepository.databinding.RepositoriesActivityBinding
import pl.kamilkulig.githubsearchingforrepository.repositorydetail.RepositoryDetailActivity

class RepositoriesActivity : AppCompatActivity(), ItemOnClickListener {

    companion object {
        const val ARRAY_KEY = "list"
        const val UNIQUE_TAG = "show"
        const val INTENT_NAME_REPO = "NAME_REPO"
    }

    private lateinit var staggeredGridLayoutManager: StaggeredGridLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: RepositoriesActivityBinding =
                DataBindingUtil.setContentView(this, R.layout.repositories_activity)
        val viewModel = ViewModelProviders.of(this,
                ViewModelFactory.getInstance()).get(RepositoriesViewModel::class.java)
        binding.viewmodel = viewModel
        repositories_recycler_view.apply {
            staggeredGridLayoutManager = StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
            layoutManager = staggeredGridLayoutManager
            adapter = viewModel.repositoriesAdapter

        }
        viewModel.searchEvent.observe(this@RepositoriesActivity, Observer {
            this@RepositoriesActivity.prepareViewToSearch()
        })
        viewModel.selectSortOptionEvent.observe(this@RepositoriesActivity, Observer {
            if (it != null) {
                openSortOptionsPicker(it)
            }
        })
        viewModel.getRepositoryDetailsEvent.observe(this@RepositoriesActivity, Observer {
            Intent(this@RepositoriesActivity, RepositoryDetailActivity::class.java).apply {
                putExtra(INTENT_NAME_REPO, it)
                startActivity(this)
            }
        })
        viewModel.messageEvent.observe(this@RepositoriesActivity, Observer {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        })
    }

    private fun prepareViewToSearch() {
        if (currentFocus != null) {
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus.windowToken,
                    InputMethodManager.RESULT_UNCHANGED_SHOWN)
            search_edit_text.clearFocus()
        }
        staggeredGridLayoutManager.scrollToPositionWithOffset(0, 0)
    }

    private fun openSortOptionsPicker(bundle: Bundle) {
        val fragment = SortOptionPickerFragment()
        fragment.arguments = bundle
        fragment.show(supportFragmentManager, UNIQUE_TAG)
    }

    override fun getItemNameOnClick(name: String) {
        Intent(this@RepositoriesActivity, RepositoryDetailActivity::class.java).apply {
            putExtra(INTENT_NAME_REPO, name)
            startActivity(this)
        }
    }
}
