package pl.kamilkulig.githubsearchingforrepository.repositories

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.sort_option_fragment.*
import pl.kamilkulig.githubsearchingforrepository.ItemOnClickListener
import pl.kamilkulig.githubsearchingforrepository.R
import android.arch.lifecycle.ViewModelProviders

class SortOptionPickerFragment : DialogFragment(), ItemOnClickListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View
            = inflater.inflate(R.layout.sort_option_fragment, container)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val bundle = arguments
        sort_option_recycler_view.layoutManager = LinearLayoutManager(this.activity)
        if (bundle != null) {
            val sortOptionList = bundle.getStringArrayList(RepositoriesActivity.ARRAY_KEY)
            sort_option_recycler_view.adapter = SortOptionAdapter(sortOptionList, this@SortOptionPickerFragment)
        } else {
            dismiss()
        }
    }

    interface OnSortOptionPass {
        fun onSortOptionPass(sortOption: String)
    }

    override fun getItemNameOnClick(name: String) {
        ViewModelProviders.of(activity!!).get(RepositoriesViewModel::class.java).sortOption.set(name)
        dismiss()
    }
}
