package pl.kamilkulig.githubsearchingforrepository.repositories

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import pl.kamilkulig.githubsearchingforrepository.data.model.Repository
import android.widget.TextView


object RepositoriesBindings {
    @BindingAdapter("onOkInSoftKeyboard") // I like it to match the listener method name
    @JvmStatic fun setOnOkInSoftKeyboardListener(view: EditText,
                                                 listener: OnOkInSoftKeyboardListener) {
        view.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                listener.onOkInSoftKeyboard()
                return@OnEditorActionListener true
            }
            false
        })
    }
}