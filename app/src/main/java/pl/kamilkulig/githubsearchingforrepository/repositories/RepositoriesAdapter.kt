package pl.kamilkulig.githubsearchingforrepository.repositories

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import pl.kamilkulig.githubsearchingforrepository.ItemOnClickListener
import pl.kamilkulig.githubsearchingforrepository.data.model.Repository
import pl.kamilkulig.githubsearchingforrepository.R
import pl.kamilkulig.githubsearchingforrepository.databinding.ItemRepoBinding

class RepositoriesAdapter(var itemListener: ItemOnClickListener) : RecyclerView.Adapter<RepositoriesAdapter.ViewHolder>() {
    private lateinit var repositories: MutableList<Repository>
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemRepoBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
                R.layout.item_repo, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(repositories[holder.adapterPosition])
    }

    override fun getItemCount(): Int = if (::repositories.isInitialized) repositories.size else 0

    fun updateRepositories(repositories: List<Repository>) {
        this.repositories = repositories.toMutableList()
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemRepoBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(repositories: Repository) {
            with(binding) {
                repo = repositories
                executePendingBindings()
            }
        }

        init {
            binding.root.setOnClickListener {
                itemListener.getItemNameOnClick(binding.repoNameTextView.text.toString())
            }
        }
    }
}