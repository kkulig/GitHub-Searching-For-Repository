package pl.kamilkulig.githubsearchingforrepository.data.model

data class SearchCriteria(val query: String, val sort: String, val order: String)