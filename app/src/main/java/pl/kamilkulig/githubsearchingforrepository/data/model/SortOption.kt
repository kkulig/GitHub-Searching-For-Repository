package pl.kamilkulig.githubsearchingforrepository.data.model

data class SortOption(val name: String, val value: String, val order: String)