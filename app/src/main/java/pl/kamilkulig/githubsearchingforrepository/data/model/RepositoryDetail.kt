package pl.kamilkulig.githubsearchingforrepository.data.model

import com.google.gson.annotations.SerializedName

data class RepositoryDetail(@SerializedName("watchers_count")
                            var watchers: Int = 0,
                            @SerializedName("stargazers_count")
                            val stars: Int = 0,
                            @SerializedName("forks_count")
                            val forks: Int = 0,
                            @SerializedName("html_url")
                            val url: String = "#") : Repository()