package pl.kamilkulig.githubsearchingforrepository.data

import pl.kamilkulig.githubsearchingforrepository.data.model.Repository
import pl.kamilkulig.githubsearchingforrepository.data.model.RepositoryDetail
import pl.kamilkulig.githubsearchingforrepository.data.model.ResultFromSearch
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RetrofitService {
    @GET("repositories")
    fun getRepositories(): Call<List<Repository>>

    @GET("search/repositories")
    fun getRepositoriesFromSearch(@Query("q") q: String, @Query("sort") sort: String,
                                  @Query("order") order: String)
            : Call<ResultFromSearch>

    @GET("repos/{owner}/{repo}")
    fun getRepositoriesDetail(@Path("owner") owner: String, @Path("repo") repo: String)
            : Call<RepositoryDetail>

    companion object Factory {
        fun create(): RetrofitService {
            val retrofit: Retrofit = Retrofit.Builder()
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("https://api.github.com")
                    .build()
            return retrofit.create(RetrofitService::class.java)
        }
    }
}
