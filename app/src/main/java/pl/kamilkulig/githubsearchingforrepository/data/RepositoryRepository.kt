package pl.kamilkulig.githubsearchingforrepository.data

import pl.kamilkulig.githubsearchingforrepository.data.model.ResultFromSearch
import pl.kamilkulig.githubsearchingforrepository.data.model.SearchCriteria
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RepositoryRepository(var webService: RetrofitService) : RepositoryDataSource {

    override fun getRepositories(searchCriteria: SearchCriteria, callback: RepositoryDataSource.LoadRepositoriesCallback) {
        webService.getRepositoriesFromSearch(searchCriteria.query, searchCriteria.sort, searchCriteria.order)
                .enqueue(object : Callback<ResultFromSearch> {
                    override fun onFailure(call: Call<ResultFromSearch>?, t: Throwable?) {
                        callback.onDataNotAvailable()
                    }

                    override fun onResponse(call: Call<ResultFromSearch>, response: Response<ResultFromSearch>) {
                        callback.onRepositoriesLoaded(response.body()?.items ?: listOf())
                    }
                })
    }

    companion object {

        private var INSTANCE: RepositoryRepository? = null

        @JvmStatic fun getInstance(retrofitService: RetrofitService) =
                INSTANCE ?: synchronized(RepositoryRepository::class.java) {
                    INSTANCE ?: RepositoryRepository(retrofitService)
                            .also { INSTANCE = it }
                }

        @JvmStatic fun destroyInstance() {
            INSTANCE = null
        }
    }
}
