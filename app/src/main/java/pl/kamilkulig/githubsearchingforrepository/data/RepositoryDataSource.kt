package pl.kamilkulig.githubsearchingforrepository.data

import pl.kamilkulig.githubsearchingforrepository.data.model.Repository
import pl.kamilkulig.githubsearchingforrepository.data.model.SearchCriteria

interface RepositoryDataSource {

    interface LoadRepositoriesCallback {

        fun onRepositoriesLoaded(items: List<Repository>)

        fun onDataNotAvailable()
    }

    fun getRepositories(searchCriteria: SearchCriteria, callback: LoadRepositoriesCallback)
}