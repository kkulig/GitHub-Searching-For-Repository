package pl.kamilkulig.githubsearchingforrepository.data.model

import com.google.gson.annotations.SerializedName

open class Repository(@SerializedName("full_name") var name: String = " ",
                      @SerializedName("description") val description: String = " ")