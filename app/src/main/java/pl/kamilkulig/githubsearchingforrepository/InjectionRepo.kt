package pl.kamilkulig.githubsearchingforrepository

import pl.kamilkulig.githubsearchingforrepository.data.RepositoryRepository
import pl.kamilkulig.githubsearchingforrepository.data.RetrofitService

object InjectionRepo {
    fun provideRepository() = RepositoryRepository.getInstance(RetrofitService.create() )
}